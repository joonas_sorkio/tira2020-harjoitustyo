package graafi;

import java.util.ArrayList;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Graafi.java                                                     #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
# Kuvaus: Graafi-tietorakenne, sisältää kaarilistan ja vierekkyyslistan     #
# Muuta:  Graafin solmut ovat Solmu.java -luokkia                           # 
#         ja kaaret Kaari.java -luokkia. Graafin tulostamiseen käytetään    #
#         solmut sisältävää vierekkyyslistaa.                               #
#############################################################################
*/
public class Graafi {
	// Alustetaan viitteitä jotka määritellään myöhemmin.
	private int solmuLkm;
   private int kaariLkm;
   private int paino;
   public ArrayList<Solmu> solmut;
   public ArrayList<Kaari> kaaret;
   private Kaari kaari;
   
   /*
    * Rakentaja asettaa uuden graafin solmujen ja kaarien lukumääräksi 0.
    * Luodaan myös uusi (vierekkyys)lista solmujen syöttöä varten. 
    */
	public Graafi() {
      solmuLkm = 0;
      kaariLkm = 0;
      solmut = new ArrayList<Solmu>();
	}
   
   /*
    * Metodi uuden kaaren luontiin ja lisäykseen. Kaari-olion sisälle
    * tiedot siihen liittyvistä kahdesta solmusta ja kaari-olio lisätään
    * molempien solmujen sisälle. 
    */
   public void addEdge(Solmu edellinen, Solmu sl, boolean onkoLoytoKaari) { 
      // Jos kaaria ei ole lisätty aiemmin, luodaan uusi kaarilista.
      if (kaariLkm < 1) {
         kaaret = new ArrayList<Kaari>();  
      }
      // Painona toimii solmujen harmaasävyjen erotus. 
      paino = Math.abs(edellinen.harmaaSavy() - sl.harmaaSavy());

      // Luodaan uusi kaari jonka solmuiksi parametrinä olevat ja painoksi
      // näiden harmaasävyjen erotus.
      kaari = new Kaari(paino, edellinen, sl, onkoLoytoKaari);
      
      // Tallennetaan kaari graafiin.
      kaaret.add(kaari);
      // Lisätään kaari molempiin solmuihin. 
      edellinen.lisaaKaari(kaari);
      sl.lisaaKaari(kaari);
      
      // Kasvatetaan kaarien lukumäärää.
      kasvataKaariLkm();
   } 
   /*
    * Metodi Solmun lisäämiseen graafiin. Jos solmuja on yksi tai enemmän
    * luodaan myös uusi kaari edellisen ja nykyisen solmun välille.
    */
   public void insertVertex(Solmu edellinen, Solmu sl) { 
      
      // Solmuja ei ole lisätty aiemmin, lisätään solmu luomatta kaarta. 
      if (solmuLkm < 1) {
         solmut.add(sl);
         kasvataSolmuLkm();
      }
      // Solmuja on enemmän kuin yksi, lisätään uusi solmu ja merkitään kaari
      // sen ja vanhan solmun välille.
      else {    
         // Lisätään uusi solmu vierekkyyslistaan.
         solmut.add(sl);
         
         // Lisätään löytökaari ja kasvatetaan solmujen lukumäärää.
         addEdge(edellinen, sl, true);
         kasvataSolmuLkm();
      }
   }
   
   public int solmuLkm() {
      return solmuLkm;
   }
   
   public void kasvataSolmuLkm() {
      solmuLkm++;
   }
      
   public int kaariLkm() {
      return kaariLkm;
   }
   
   public void kasvataKaariLkm() {
      kaariLkm++;
   }
} 
