package graafi;

import java.util.ArrayList;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Solmu.java                                                      #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
# Kuvaus: Solmu-luokka joka luodaan jokaiselle kuvan pisteelle. Sisältää    #
#         tiedot kuvapisteen harmaasävystä, koordinaateista ja siitä onko   #
#         sitä aiemmin käsitelty. Merkitään myös solmuun liittyvät kaaret   #
#         vierekkyyslista-formaatissa.                                      #
# Muuta:  Koko ohjelman keskeisin luokka, hyödynnetään melkein jokaisen     #
#         muun luokan yhteidessä.                                           #
#############################################################################
*/
public class Solmu {
   
   // Alustetaan viitteitä. 
   private int harmaaSavy;
   private int koordX;
   private int koordY;
   private boolean katsottu;
   public ArrayList<Kaari> kaaret; 
   
   // Kolmiparametrinen rakentaja, Solmuun sijoitetaan koordinaatit, harmaaSavy
   // Ja tieto siitä, onko siinä käyty. Luodaan myös uusi lista solmusta
   // lähteville kaarille. 
   public Solmu(int uusiSavy, int uusiKoordX, int uusiKoordY, boolean uusiKatsottu) {
      harmaaSavy(uusiSavy);
      koordX(uusiKoordX);
      koordY(uusiKoordY);
      katsottu(uusiKatsottu);
      kaaret = new ArrayList<Kaari>(); 
	}
   /*
    * Getterit ja Setterit. 
    */
   public int koordX() {
      return koordX;
   }
   public void koordX(int uusiKoordX) {
      koordX = uusiKoordX;
   }
   public int koordY() {
      return koordY;
   }
   public void koordY(int uusiKoordY) {
      koordY = uusiKoordY;
   }
   public int harmaaSavy() {
      return harmaaSavy;
   }
   public void harmaaSavy(int uusiSavy) {
      harmaaSavy = uusiSavy;
   }
   public boolean katsottu() {
      return katsottu;
   }
   public void katsottu(boolean uusiKatsottu) {
      katsottu = uusiKatsottu;
   }
   public void lisaaKaari(Kaari uusiKaari) {
      kaaret.add(uusiKaari);
   }
}
