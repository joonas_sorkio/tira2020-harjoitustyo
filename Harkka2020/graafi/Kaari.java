package graafi;

import java.util.ArrayList;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Kaari.java                                                      #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
# Kuvaus: Kaari-luokka joka luodaan kahden toisiinsa liittyvän Solmun       #
#         yhteyteen. Sisältää kaareen liittyvät solmut                      #
#         kaarilista-formaatissa ja näiden välisen painon (harmaasävyn ero) #
# Muuta: -                                                                  #
#############################################################################
*/
public class Kaari {
   // Viitteet solmut sijaitsevaan listaan ja painoon. 
   public ArrayList<Solmu> solmut; 
   private int paino;
   private boolean loytoKaari;
   
   // Kolmiparametrinen rakentaja. Kaareen sijoitetaan molemmat solmut ja näiden
   // välinen paino.
   public Kaari(int uusiPaino, Solmu ekaSolmu, Solmu tokaSolmu, boolean onkoLoytoKaari) {
      paino(uusiPaino);
      loytoKaari(onkoLoytoKaari);
      solmut = new ArrayList<Solmu>();
      solmut.add(ekaSolmu);
      solmut.add(tokaSolmu);
	}
   
   public int paino() {
      return paino;
   }
   public void paino(int uusiPaino) {
      paino = uusiPaino;
   }
   public boolean loytoKaari() {
      return loytoKaari;
   }
   public void loytoKaari(boolean onkoLoytoKaari) {
      loytoKaari = onkoLoytoKaari;
   }
}
