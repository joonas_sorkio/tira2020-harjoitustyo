import hakualgoritmit.Leveyshaku;
import hakualgoritmit.Syvyyshaku;
import hakualgoritmit.MinimivirittavaPuu;
import graafi.Graafi;
import graafi.Kaari;
import graafi.Solmu;
import aputietorakenteet.MinHeap;

import java.io.FileInputStream; 
import java.io.FileNotFoundException; 
import javafx.application.Application; 
import javafx.scene.Group; 
import javafx.scene.Scene; 
import javafx.scene.image.Image;  
import javafx.scene.image.PixelReader; 
import javafx.scene.image.PixelWriter; 
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color; 
import javafx.scene.image.ImageView; 
import javafx.stage.Stage;  
import javafx.scene.control.Button;
import javafx.scene.control.TextField; 
import javafx.scene.text.Text; 
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent; 
import javafx.event.ActionEvent;
import java.io.FileWriter; 
import java.io.IOException;
import java.util.ArrayList;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Harkka2020.java                                                 #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi), Käyttöliittymän pohjan on #
#          muodostanut opettaja Jyrki Rasku (jyrki.rasku@tuni.fi).          #
#          Käyttöliittymä muokattu                                          #
#          https://www.tutorialspoint.com/javafx/index.htm esimerkeistä.    #
# Kuvaus: Ohjelma valokuvan yhtenäisten komponenttien muodostamiseen        #
#         graafin avulla eri hakualgoritmejä käyttäen. Harkka2020.java      #
#         sisältää ohjelman pääluokan ja käyttöliittymän.                   #
# Muuta:  Hakualgoritmien toteutus muissa luokissa (Syvyyshaku.java,        #
#         Leveyshaku.java, MinimivirittavaPuu.java)                         #
#############################################################################
*/
public class Harkka2020 extends Application {  
   
   // Alustetaan viitteitä myöhempään käyttöön.
   Button button1;
   Button button2;
   Button button3;
   Button button5;
   Image image;
   WritableImage wImage;
   double width;
   double height;
   ImageView imageView1;
   ImageView imageView2;
   ArrayList<ArrayList<Solmu>> harmaaSavyTaulu;
   int klikattuX;
   int klikattuY;
   Graafi gr;
   Kaari kaari;
   Solmu sl;
   Solmu sl1;
   Solmu sl2;
   String txt1;
   String txt2;
   int maxIntensiteetti;
   int maxKokonaisEro;
   MinHeap mh;
   
   @Override 
   public void start(Stage stage) throws FileNotFoundException {         
      
      // Luodaan uusi kuva tiedostosta ja asetetaan vasemmanpuoleiseen näkymään.  	  
      image = new Image(new FileInputStream("Testikuva.jpg"));  
      width=image.getWidth();
	   height=image.getHeight();	   
      imageView1 = new ImageView(image); 
      
      // Asetetaan ensimmäisen kuvanäkymän sijainti käyttöliittymässä.
      // HUOM! Tämä vaikuttaa hiiren koordinaatteihin kuvassa.
      imageView1.setX(50); 
      imageView1.setY(25); 
      
      // Skaalataan kuva sopivaksi vasemmanpuoleiseen kuvanäkymään. 
      imageView1.setFitHeight(height/2); 
      imageView1.setFitWidth(width/2);         
      
      // Säilytetään alkuperäinen kuvasuhde. 
      imageView1.setPreserveRatio(true); 
         
      // Luodaan seuraava kuva joka alustetaan ensiksi samaksi kuin alkuperäinen.
      // Myöhemmin tähän kuvanäkymään muodostetaan harmaasävykuva. 
      imageView2 = new ImageView(image);
      
      // Asetetaan toisenkuvanäkymän sijainti käyttöliittymässä.
      imageView2.setX(width/2+60); 
      imageView2.setY(25); 
      
      // Skaalataan kuva sopivaksi oikeanpuoleiseen kuvanäkymään. 
      imageView2.setFitHeight(height/2); 
      imageView2.setFitWidth(width/2);          
      
      // Säilytetään alkuperäinen kuvasuhde. 
      imageView2.setPreserveRatio(true); 
      
      // Luodaan tekstikentät halutun intensiteetti-ja kokonaiseron 
      // selvittämiseksi.
      int delta=50;
      Text text1 = new Text("Anna sallittu intensiteettiero");  
      text1.setLayoutX(50);
      text1.setLayoutY(height/2+delta);
      text1.setFill(Color.WHITE);
      TextField textField1 = new TextField(); 
      textField1.setText("30");
      textField1.setLayoutX(50);
      textField1.setLayoutY(height/2+1.1*delta);
	  
      Text text2 = new Text("Anna sallittu kokonaisero");  
      text2.setLayoutX(50);
      text2.setLayoutY(height/2+2.4*delta);
      text2.setFill(Color.WHITE);
      TextField textField2 = new TextField();
      textField2.setText("35");
      textField2.setLayoutX(50);
      textField2.setLayoutY(height/2+2.5*delta);
	  
      // Luodaan nappulat hakualgoritmien kutsumiselle. 
      button1 = new Button("Hae yksi komponentti syvyyshaulla");
      button1.setLayoutX(600);
      button1.setLayoutY(height/2+1.1*delta);
      button1.setDisable(true);
	  
      button2 = new Button("Hae yksi komponentti leveyshaulla");
      button2.setLayoutX(600);
      button2.setLayoutY(height/2+2.5*delta);
      button2.setDisable(true);
     
      button3 = new Button("Hae yhden komponentin minimikaaret");
      button3.setLayoutX(920);
      button3.setLayoutY(height/2+1.1*delta);
      button3.setDisable(true);
          
      // Nappula jolla voidaan halutessa palauttaa tyhjä harmaasävykuva hakujen
      // välissä. 
      button5 = new Button("Palauta alkuperäinen harmaasävyesitys");
      button5.setLayoutX(920);
      button5.setLayoutY(height/2+2.5*delta);

      // Yleinen handleri nappuloita ja tekstikenttiä varten.
      EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() { 
         public void handle(ActionEvent e) { //Luetaan tekstikenttien tiedot.    
            // Otetaan syötteet intensiteetin ja kokonaiseron tekstikentistä
            // ja käännetään kokonaislukumuotoon.
            txt1=textField1.getText();
            txt2=textField2.getText();
            maxIntensiteetti = Integer.parseInt(txt1);
            maxKokonaisEro = Integer.parseInt(txt2);
            
            // Valitaan suoritettava tehtävä.
            if(e.getSource()== button1)
               Syvyyshaku(); 
            if(e.getSource()== button2)
            	Leveyshaku();
            if(e.getSource()== button3)
            	MinVPuu();			  
            // Palauttaa alkuperäisen harmaasävykuvan käyttäjän halutessa.
            if(e.getSource()== button5)
            	ManipulateImage();
         }       
      }; 
	  
      button1.setOnAction(event);
      button2.setOnAction(event);
      button3.setOnAction(event);
      button5.setOnAction(event);
     
      // Handleri, jossa käsitellään hiiren klikkaus kuvanäkymissä. 
      EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() { 
         @Override 
         public void handle(MouseEvent e) {             
            // Haetaan alustavat hiiren koordinaatit doublena.
            double x = e.getX();
            double y = e.getY(); 
            // Käännetään koordinaatit kokonaisluvuksi ja vähennetään marginaalit.
            klikattuX = (int)(x - 50) * 2;
            klikattuY = (int)(y - 25) * 2;
            // Jos klikkaus osuu oikeanpuolimmaiseen kuvaan, säädetään
            // X-koordinaattia, jotta se vastaisi samaa kuvan kohtaa. 
            if (klikattuX > 1080) {
               klikattuX = klikattuX - 1100;
            }
            // Tulostaa komentoliittymään valmiit koordinaatit.
            System.out.println("Klikattu kuvan kohtaa X:"+ klikattuX 
                               + " ja Y:"+ klikattuY);
         } 
      };  
      
      // Liitetään hiiren händleri kuvanäkymiin. Myös toista kuvaa voi klikkailla.
      imageView1.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
     	imageView2.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
      
      // Ryhmitetään käyttöliittymäelementit.
      Group root = new Group(imageView1, imageView2, text1, textField1, 
      text2, textField2,button1,button2,button3,button5);  
      
      // Luodaan scene -olio jossa ylläolevat käyttöliittymäelementit. 
      Scene scene = new Scene(root, width+100, height*0.85, Color.rgb(40, 40, 40));  
      
      // Asetetaan uusi otsikko
      stage.setTitle("Tietorakenteet 2020: Harjoitustyö");  
      
      // Lisätään scene stageen.
      stage.setScene(scene);  
      
      // Näytetään näkymä käyttäjälle.
      stage.show();
      
      // Alustetaan harmaasävykuva (ja taulukkolista) kerran ohjelman alussa.
      ManipulateImage(); 
   }  
   /*
    * Metodi, jossa luodaan harmaasävykuva alkuperäisestä ja muodostetaan
    * Solmuja sisältävät taulukkolista. 
    */
   public void ManipulateImage() {
	   
      // Sijoitetaan uusi kirjoituskuva wImage-viitteeseen.
      wImage = new WritableImage((int)width, (int)height);
      PixelReader pixelReader = image.getPixelReader();      
      PixelWriter writer = wImage.getPixelWriter();
      
      // Luodaan uusi 2D-taulukkolista, päädyin taulukkolistaan koska se oli
      // nähdäkseni helpoin tapa syöttää siihen suoraan kuvapisteitä vastaavia
      // Solmuja, mikä taas helpottaa "käytyjen" solmujen merkkaamista. 
      harmaaSavyTaulu = new ArrayList<ArrayList<Solmu>>();
      
      // Silmukka jossa käydään kuvapisteet läpi yksitellen. 
      for(int x = 0; x < width; x++) {
         // Luodaan jokaiselle sarakkeelle (x-koordinaatti) vastaava taulukkolista. 
         harmaaSavyTaulu.add(new ArrayList<Solmu>()); 
         // Käsitellään yksittäinen sarake
         for(int y = 0; y < height; y++) { 
            
            // Otetaan talteen alkuperäisen pikselin väri kuvasta. 
            Color color = pixelReader.getColor(x, y); 
            
            // Laskutoimitukset harmaasävyä varten
            int r = (int) Math.round(color.getRed() * 255);
            int g = (int) Math.round(color.getGreen() * 255);
            int b = (int) Math.round(color.getBlue() * 255);
            int harmaaSavy = (int) Math.round(0.3*r+0.59*g+0.11*b);
            
            // Piirretään kuvaan mustavalkokuvaa vastaava harmaasävyinen pikseli.
            writer.setColor(x, y, Color.rgb(harmaaSavy,harmaaSavy,harmaaSavy));
            
            // Luodaan pikseliä vastaava uusi solmu johon sijoitetaan nykyisen 
            // harmaasävyn lisäksi koordinaatit ja totuusmuuttuja katsomisen
            // merkkaamista varten.
            sl = new Solmu(harmaaSavy, x, y, false);
            
            // Sijoitetaan solmu taulukkolistaan vastaavalle paikalle.
            harmaaSavyTaulu.get(x).add(sl);
            
         }
      }	
      
      // Sallitaan jälleen nappuloiden käyttö mikäli se estettiin aikaisemmin.
      button1.setDisable(false);
      button2.setDisable(false);
      button3.setDisable(false);
      
      // Sijoitetaan harmaasävykuva oikeanpuolimmaiseen kuvanäkymään
      imageView2.setImage(wImage);

   }
   /*
    * Metodi syvyyshaun kutsuntaan ja tulostamiseen.
    */
   public void Syvyyshaku() {
      // Luodaan uusi syvyyshakuolio ja muodostetaan syvyyshaun avulla 
      // komponentin sisältävä graafi.
      Syvyyshaku syvyyshaku = new Syvyyshaku();
      gr = syvyyshaku.kaynnistaSyvyyshaku(maxIntensiteetti, maxKokonaisEro, 
                                          harmaaSavyTaulu, klikattuX, klikattuY);
      // Kutsutaan tulostamismetodia.
      piirraGraafiKuvaan(gr, 0);
      /*
      for (int ind = 0; ind < gr.kaariLkm(); ind++) {
         System.out.println (ind + "Kaaren paino" + gr.kaaret.get(ind).paino());
      }
      */
   }
   /*
    * Metodi leveyshaun kutsuntaan ja tulostamiseen.
    */
   public void Leveyshaku() {
      // Luodaan uusi leveyshakuolio ja muodostetaan leveyshaun avulla 
      // komponentin sisältävä graafi.
      Leveyshaku leveyshaku = new Leveyshaku();
      gr = leveyshaku.kaynnistaLeveyshaku(maxIntensiteetti, maxKokonaisEro, 
                                          harmaaSavyTaulu, klikattuX, klikattuY);
      // Kutsutaan tulostamismetodia.
      piirraGraafiKuvaan(gr, 1);
   }
   
   public void MinVPuu() {
      
      // Muodostetaan ensin graafi jonka kaaria voidaan sitten käsitellä.
      Syvyyshaku syvyyshaku = new Syvyyshaku();
      gr = syvyyshaku.kaynnistaSyvyyshaku(maxIntensiteetti, maxKokonaisEro, 
                                          harmaaSavyTaulu, klikattuX, klikattuY);

      // Käydään luomassa minimivirittävä puu, säiliönä toimii minimikeko. 
      MinimivirittavaPuu minVPuu = new MinimivirittavaPuu();
      mh = minVPuu.muodostaMinVPuu(gr); 

      System.out.println("MinimiVPiirtoa on kutsuttu.");
      
      // Alustetaan lukija ja kirjoittaja.
      PixelReader pixelReader = image.getPixelReader();      
      PixelWriter writer = wImage.getPixelWriter();  
      
      // Silmukka minimipuun piirtämiseen painojärjestyksessä. Kaarien
      // solmut tulostetaan painoa vastaavissa väreissä.  
      for (int ind = 0; ind < gr.kaariLkm(); ind++) {  
         
         // Haetaan seuraava pienin kaari keosta sekä sen solmut.
         kaari = mh.extractMin();
         sl1 = kaari.solmut.get(0);
         sl2 = kaari.solmut.get(1);
         
         // Piirretään kaarten solmut eri värillä painojen(harmaasävyero) mukaan.
         if (kaari.paino() <= 1) {
            writer.setColor(sl1.koordX(), sl1.koordY(), Color.rgb(255, 0, 0));
            writer.setColor(sl2.koordX(), sl2.koordY(), Color.rgb(255, 0, 0));
         }
         else if (kaari.paino() <= 4) {
            writer.setColor(sl1.koordX(), sl1.koordY(), Color.rgb(255, 169, 0));
            writer.setColor(sl2.koordX(), sl2.koordY(), Color.rgb(255, 169, 0));
         }
         else if (kaari.paino() <= 8) {
            writer.setColor(sl1.koordX(), sl1.koordY(), Color.rgb(255, 255, 0));
            writer.setColor(sl2.koordX(), sl2.koordY(), Color.rgb(255, 255, 0));
         }
         else if (kaari.paino() <= 14) {
            writer.setColor(sl1.koordX(), sl1.koordY(), Color.rgb(0, 255, 0));
            writer.setColor(sl2.koordX(), sl2.koordY(), Color.rgb(0, 255, 0));
         }
         else {
            writer.setColor(sl1.koordX(), sl1.koordY(), Color.rgb(255, 0, 255));
            writer.setColor(sl2.koordX(), sl2.koordY(), Color.rgb(255, 0, 255));
         }
      } 
      
      // Päivitetään kuvanäkymä.
      imageView2.setImage(wImage);
      
      System.out.println("Päästiin MinimiVPiirron loppuun.");
      System.out.println("----------------------------------");
   }
   
   public void Kaikki() {
      try {
         FileWriter myWriter = new FileWriter("Graafi.txt");
         for(int i=0; i<10; i++)
         myWriter.write("Rivi"+String.valueOf(i)+"\n");
         myWriter.close();
      
      }    
      catch (IOException e) {
         System.out.println("An error occurred.");
         e.printStackTrace();
      }
   }
   /*
    * Metodi graafikomponentin piirtoon kuvassa. Tässä vaiheessa hakualgoritmit
    * on jo suoritettu ja kaikki graafin solmut tulostetaan suoraan 
    * graafin sisällä olevasta taulukkolistasta. 
    */
   public void piirraGraafiKuvaan(Graafi gr, int vari) {
      
      System.out.println("Piirtoa on kutsuttu."); 
      // Alustetaan lukija ja kirjoittaja.
      PixelReader pixelReader = image.getPixelReader();      
      PixelWriter writer = wImage.getPixelWriter();  
      
      // Tulostetaan graafin kaikki solmut suoraan graafissa olevasta solmujen
      // taulukkolistasta jotta syvyys- tai leveyshakua ei tarvitsisi suorittaa 
      // enää uudelleen.        
      for (int ind = 0; ind < gr.solmuLkm(); ind++) { 
         sl = gr.solmut.get(ind);   
         // Jos tulostetaan syvyyshakugraafia, piirretään punaisella.
         if (vari == 0) {
            writer.setColor(sl.koordX(), sl.koordY(), Color.RED);
         }
         // Jos tulostetaan leveyshakugraafia, piirretään vihreällä.
         else if (vari == 1) {
            writer.setColor(sl.koordX(), sl.koordY(), Color.GREEN);   
         }
      } 
      
      // Päivitetään kuvanäkymä.
      imageView2.setImage(wImage);
      
      System.out.println("Päästiin piirron loppuun.");
      System.out.println("----------------------------------");
   }
   
   public static void main(String args[]) { 
      launch(args); 
   } 
}        
