package hakualgoritmit;

import aputietorakenteet.DynArrStack;
import graafi.Solmu;
import graafi.Graafi;
import graafi.Kaari;
import java.util.ArrayList;
/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Syvyyshaku.java                                                 #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
# Kuvaus: Syvyyshaku-luokka joka sisältää hakualgoritmin graafin            #
#         yhtenäisten komponenttien muodostamiseen kuvasta syvyyshaulla.    #
# Muuta:  Hyödyntää pinoa (DynArrStack.java), graafia (Graafi.java) ja      #
#         Solmuja (Solmu.java).                                             #
#############################################################################
*/
public class Syvyyshaku {
   // Viite graafiin.
   private Graafi gr;
   // Viite pinoon.
   private DynArrStack pino;
   // Viite nykyiseen solmuun.
   private Solmu sl;
   // Viite edelliseen solmuun, käytetään kaarien muodostamisessa. 
   private Solmu edellinen;
   
   /*
    * Rakentajassa ei toistaiseksi alusteta mitään arvoja. 
    */
	public Syvyyshaku() {
   }
   
   /*
    * Metodi syvyyshaun käynnistämiseen. Luodaan uusi graafi, pino ja merkitään
    * lähtösolmu. 
    */
   public Graafi kaynnistaSyvyyshaku(int maxIntensEro, int maxKokonaisEro, 
         ArrayList<ArrayList<Solmu>> harmaaSavyTaulu, int alkuX, int alkuY) {
      
      // Alustetaan uusi Graafi.
      gr = new Graafi();
       
      // Merkitään lähtösolmuksi harmaasävytaulun koordinaatteja vastaava Solmu.
      sl = harmaaSavyTaulu.get(alkuX).get(alkuY);
      
      System.out.println("----------------------------------");
      System.out.println("INTENSITEETTIERO: " + maxIntensEro);
      System.out.println("KOKONAISERO: " + maxKokonaisEro);
      
      // Merkitään alkuperäisen harmaasävyn arvo muistiin, jotta tätä voidaan
      // käyttää harmaasävyn intensiteetin kokonaiseron vertailussa.
      int alkupHarmaa = sl.harmaaSavy();
      
      // Luodaan uusi pino syvyyshakua varten.
      pino = new DynArrStack();

      System.out.println("Syvyyshaku käynnistetty");
      
      // Kutsutaan muodostaGraafi-metodia jossa varsinainen syvyyshaku tapahtuu.
      muodostaGraafi(maxIntensEro, maxKokonaisEro, alkupHarmaa, sl, 
                         harmaaSavyTaulu);
      
      // Palautetaan valmis graafi. 
      return gr;
   } 
   /*
    * Sijoitetaan graafiin kaikki solmut, jotka täyttävät reunaehdot syvyyshaun
    * avulla.
    */
   public void muodostaGraafi(int maxIntensEro, int maxKokonaisEro, 
                              int alkupHarmaa, Solmu sl, 
                              ArrayList<ArrayList<Solmu>> harmaaSavyTaulu) {
      
      System.out.println("EkaSolmuX: " + sl.koordX());
      System.out.println("EkaSolmuY: " + sl.koordY());
      
      // Lippumuuttuja solmun koordinaattien oikeellisuuden seurantaan.
      boolean validiKoordinaatti = true;  
      
      // Silmukka jonka sisällä syvyyshaku tapahtuu, ajetaan vähintään kerran ja
      // tämän jälkeen niin kauan kun pinossa on vielä solmuja jäljellä. 
      do {
         // Oletetaan, että nykyisen solmun koordinaatit ei ole kuvan reunassa.
         validiKoordinaatti = true;
         
         // Jos nykyinen solmu on liian lähellä kuvan reunaa,
         // estetään jatkotoimenpiteet lippumuuttujan avulla.
         if ((sl.koordX() > 1078) || (sl.koordX() < 1) || 
             (sl.koordY() > 1078) || (sl.koordY() < 1)) {
            validiKoordinaatti = false;
         }
         // Pusketaan ensimmäinen solmu pinoon.
         if (gr.solmuLkm() < 1) {
            pino.push(sl);
            // Merkataan edelliseksi solmuksi nykyinen. 
            edellinen = sl;
            // Lisätään solmu graafiin, tässä poikkeustapauksessa luomatta kaarta.
            gr.insertVertex(edellinen, sl);
            // Merkataan nykyinen solmu käydyksi.
            sl.katsottu(true);
            System.out.println("Eka solmu syötetty");  
         }
         // Katsotaan täyttääkö seuraava solmu oikealla puolella reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY())
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.
            harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla.
            edellinen = sl;
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY());
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon. 
            gr.insertVertex(edellinen, sl);
            pino.push(sl);
            
            // System.out.println("Edetty Itään");
            
         }
         // Katsotaan täyttääkö seuraava solmu oikealla alhaalla reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()+1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()+1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()+1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.
            harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()+1).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla.
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()+1);
                    
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Kaakkoon");
         }
         // Katsotaan täyttääkö seuraava solmu alapuolella reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY()+1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY()+1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()+1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                        
            harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()+1).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla.    
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()+1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Etelään.");
            
         }
         // Katsotaan täyttääkö seuraava solmu vasemmalla alhaalla reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()+1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()+1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()+1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                                   
            harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()+1).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla.           
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()+1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.            
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Lounaaseen.");
            
         }
         // Katsotaan täyttääkö seuraava solmu vasemmalla reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY())
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                       
            harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla.  
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.            
            sl = harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY());
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Länteen.");
            
         }
         // Katsotaan täyttääkö seuraava solmu vasemmalla ylhäällä reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()-1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()-1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()-1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                       
            harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()-1).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla.             
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.             
            sl = harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()-1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Luoteeseen.");
            
         }
         // Katsotaan täyttääkö seuraava solmu yläpuolella reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY() - 1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY() - 1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()-1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                      
            harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()-1).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla. 
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()-1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Pohjoiseen.");
         }
         // Katsotaan täyttääkö seuraava solmu oikealla ylhäällä reunaehdot.
         else if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY() - 1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY() - 1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()-1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                        
            harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()-1).katsottu(true);
            
            // Merkataan uusi edellinen solmu nykyisellä solmulla. 
            edellinen = sl;
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()-1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös pinoon.
            gr.insertVertex(edellinen, sl); 
            pino.push(sl);
            
            // System.out.println("Edetty Koilliseen.");
         }
         
         // Nykyisen Solmun viereisistä solmuista ei löydetty osumia, joten
         // palataan taaksepäin. 
         else {
            // Merkataan nykyinen solmu käydyksi.
            sl.katsottu(true);
            
            // Kun pinossa on vielä alkioita..
            if (pino.isEmpty() == false) {
               // Poistetaan ensin pinosta päällimmäinen solmu johon ei palata.
               Solmu kayty = pino.pop();
               // Lisätään perääntymiskaari
               gr.addEdge(edellinen, sl, false);
               // Merkitään nykyiseksi solmuksi edellinen solmu.
               sl = edellinen; 
               
               if (pino.isEmpty() == false) {
                  // Merkataan edelliseksi solmuksi pinon päällimmäinen solmu
                  // jota ei kuitenkaan vielä poisteta. 
                  edellinen = pino.peek();
                  
               }
               
               // System.out.println("Palattu taaksepäin");
            }
         } 
      }
      while(pino.isEmpty() == false);
      
      System.out.println("Syvyyshaku valmis.");
   }
   
} 
