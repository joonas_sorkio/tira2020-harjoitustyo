package hakualgoritmit;

import graafi.Solmu;
import graafi.Graafi;
import graafi.Kaari;
import aputietorakenteet.MinHeap;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: MinimivirittavaPuu.java                                         #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
# Kuvaus: MinimivirittavaPuu-luokka joka muodostaa minimivirittavan puun    #
#          valmiista (syvyyshaulla luodusta) graafista. Aputietorakenteena  #
#          rakenteena käytetään minimikekoa.                                 #
# Muuta:  Hyödyntää Syvyyshakua(Syvyyshaku.java, Kaaria(Kaari.java) ja      #
#         Solmuja (Solmu.java).                                             #
#############################################################################
*/
public class MinimivirittavaPuu {

   MinHeap mh;
   
   /*
    * Rakentajassa ei toistaiseksi alusteta mitään arvoja. 
    */
	public MinimivirittavaPuu() {
   }
   
   /*
    * Metodi Minimivirittävän puun muodostamiseen. Pohjana käytetään aiemmin
    * syvyyshaussa muodostettua graafia, jonka kaaret laitetaan järjestykseen
    * painon (kaaren solmujen harmaasävyn erotuksen) mukaan.
    */
   public MinHeap muodostaMinVPuu(Graafi gr) {
      
      System.out.println("Aloitetaan Minimipuun muodostus");
      
      // Luodaan uusi keko
      mh = new MinHeap(gr.kaariLkm());
      
      // Silmukka jonka sisällä kaaret sijoitetaan minimikekoon. 
      for (int ind = 0; ind < gr.kaariLkm(); ind++) {
         // Katsotaan että ainakaan toisessa kaaren solmuista ei ole vielä käyty.
         // Tällä pyritään välttämään syklejä, en ole varma toimiiko se täysin. 
         // HUOM! katsottu-lippumuuttujan arvot on täällä käänteisiä, 
         // koska ne on graafia aiemmin luodessa jo käännetty.
         if (gr.kaaret.get(ind).solmut.get(0).katsottu() == true ||
             gr.kaaret.get(ind).solmut.get(1).katsottu() == true) {
            
            // Syötetään kaari kekoon. 
            mh.insert(gr.kaaret.get(ind));
            // Merkataan solmut katsotuiksi. 
            gr.kaaret.get(ind).solmut.get(0).katsottu(true);
            gr.kaaret.get(ind).solmut.get(1).katsottu(true);
         }
      }
      
      System.out.println("Minimipuu valmis"); 
      System.out.println("Minimipuun koko: " + mh.elements());
      
      // Palautetaan keko jossa kaaret järjestettynä painon mukaan.
      return mh;
   } 
} 
