package hakualgoritmit;

import aputietorakenteet.Queue;
import graafi.Solmu;
import graafi.Graafi;
import graafi.Kaari;
import java.util.ArrayList;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Leveyshaku.java                                                 #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
# Kuvaus: Leveyshaku-luokka joka sisältää hakualgoritmin graafin            #
#         yhtenäisten komponenttien muodostamiseen kuvasta leveyshaulla.    #
# Muuta:  Hyödyntää jonoa (Queue.java), graafia (Graafi.java) ja            #
#         Solmuja (Solmu.java).                                             #
#############################################################################
*/
public class Leveyshaku {

   private Graafi gr;
   private Queue jono;
   private Solmu sl;
   private Solmu edellinen;

   /*
    * Rakentajassa ei toistaiseksi alusteta mitään arvoja. 
    */
	public Leveyshaku() {
   }
   
   /*
    * Metodi leveysshaun käynnistämiseen. Luodaan uusi graafi, jono ja merkitään
    * lähtösolmu. 
    */
   public Graafi kaynnistaLeveyshaku(int maxIntensEro, int maxKokonaisEro, 
         ArrayList<ArrayList<Solmu>> harmaaSavyTaulu, int alkuX, int alkuY) {
      
      // Alustetaan uusi Graafi.
      gr = new Graafi();
      // Merkitään lähtösolmuksi harmaasävytaulun koordinaatteja vastaava Solmu.
      sl = harmaaSavyTaulu.get(alkuX).get(alkuY);
      System.out.println("----------------------------------");     
      System.out.println("INTENSITEETTIERO:" + maxIntensEro);
      System.out.println("KOKONAISERO:" + maxKokonaisEro);
      
      // Merkitään alkuperäisen harmaasävyn arvo muistiin, jotta tätä voidaan
      // käyttää harmaasävyn intensiteetin kokonaiseron vertailussa.
      int alkupHarmaa = sl.harmaaSavy();
      
      // Luodaan uusi jono syvyyshakua varten.
      jono = new Queue();

      System.out.println("Leveyshaku käynnistetty");
      
      // Kutsutaan muodostaGraafi-metodia jossa varsinainen leveyshaku tapahtuu.
      muodostaGraafi(maxIntensEro, maxKokonaisEro, alkupHarmaa, sl, 
                         harmaaSavyTaulu);
      // Palautetaan valmis graafi.     
      return gr;
   } 
   
   public void muodostaGraafi(int maxIntensEro, int maxKokonaisEro, 
                              int alkupHarmaa, Solmu sl, 
                              ArrayList<ArrayList<Solmu>> harmaaSavyTaulu) {
      
         
      System.out.println("EkaSolmuX: " + sl.koordX());
      System.out.println("EkaSolmuY: " + sl.koordY());
      // Lippumuuttuja solmun koordinaattien oikeellisuuden seurantaan.   
      boolean validiKoordinaatti = true;
      // Lippumuuttuja sille onko edeltävä solmu jo merkattu.
      boolean edellinenMerkattu = true;
      
      // Syötetään ensimmäinen solmu jonoon.
      if (gr.solmuLkm() < 1) {
         jono.enqueue(sl);
         // Merkataan edelliseksi solmuksi nykyinen. 
         edellinen = sl;
         // Lisätään solmu graafiin, tässä poikkeustapauksessa luomatta kaarta.   
         gr.insertVertex(edellinen, sl);
         // Merkataan nykyinen solmu käydyksi.
         sl.katsottu(true);
         System.out.println("Eka solmu syötetty");
            
      }
      
      // Silmukka jonka sisällä leveyshaku tapahtuu, ajetaan vähintään kerran ja
      // tämän jälkeen niin kauan kun jonossa on vielä solmuja jäljellä. 
      do {
         // Poistetaan nykyinen solmu jonon alusta.
         sl = jono.dequeue();
         
         // Lisätään vastakkaiskaari
         gr.addEdge(edellinen, sl, false);
         
         // Oletetaan, että nykyisen solmun koordinaatit ei ole kuvan reunassa.
         validiKoordinaatti = true;
         // Oletetaan, että edellistä Solmua ei ole vielä merkattu. 
         edellinenMerkattu = false;
         
         // Jos nykyinen solmu on liian lähellä kuvan reunaa,
         // estetään jatkotoimenpiteet lippumuuttujan avulla.
         if ((sl.koordX() > 1076) || (sl.koordX() < 3) || 
             (sl.koordY() > 1076) || (sl.koordY() < 3)) {
            validiKoordinaatti = false;
         }
          // Katsotaan täyttääkö seuraava solmu oikealla puolella reunaehdot.        
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY())
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.         
            harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on. 
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY());
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon. 
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Itään");
            
         }
         // Katsotaan täyttääkö seuraava solmu oikealla alhaalla reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()+1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY()+1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()+1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.         
            harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()+1).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.
            sl = harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()+1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Kaakkoon");
         }
         // Katsotaan täyttääkö seuraava solmu alapuolella reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY()+1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY()+1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()+1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                                    
            harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()+1).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.            
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
             // Haetaan uusi nykyinen solmu harmaasävytaulusta. 
            sl = harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()+1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Etelään.");
            
         }
         // Katsotaan täyttääkö seuraava solmu vasemmalla alhaalla reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()+1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()+1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()+1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                                   
            harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()+1).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.    
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.             
            sl = harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()+1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Lounaaseen.");
            
         }
         // Katsotaan täyttääkö seuraava solmu vasemmalla reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY())
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                        
            harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.   
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.          
            sl = harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY());
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Länteen.");
            
         }
         // Katsotaan täyttääkö seuraava solmu vasemmalla ylhäällä reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()-1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()-1)
                  .get(sl.koordY()-1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()-1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa. 
            harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()-1).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.  
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.                 
            sl = harmaaSavyTaulu.get(sl.koordX()-1).get(sl.koordY()-1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Luoteeseen.");
            
         }
         // Katsotaan täyttääkö seuraava solmu yläpuolella reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY() - 1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX())
                  .get(sl.koordY() - 1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()-1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                      
            harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()-1).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.  
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.            
            sl = harmaaSavyTaulu.get(sl.koordX()).get(sl.koordY()-1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Pohjoiseen.");
         }
         // Katsotaan täyttääkö seuraava solmu oikealla ylhäällä reunaehdot.
         if (validiKoordinaatti && harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY() - 1).katsottu() == false && maxIntensEro >= 
                  Math.abs(sl.harmaaSavy() - harmaaSavyTaulu.get(sl.koordX()+1)
                  .get(sl.koordY() - 1).harmaaSavy()) && maxKokonaisEro >= 
                  Math.abs(harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()-1)
                  .harmaaSavy() - alkupHarmaa)) {
            
            // Merkataan solmu käydyksi harmaasävytaulussa.                                  
            harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()-1).katsottu(true);
            
            // Katsotaan, jos edellinen alkio on jo merkattu. Merkataan 
            // edelliseksi nykyinen jos näin on.             
            if (!edellinenMerkattu) {
               edellinen = sl;
               edellinenMerkattu = true;
            }
            // Haetaan uusi nykyinen solmu harmaasävytaulusta.             
            sl = harmaaSavyTaulu.get(sl.koordX()+1).get(sl.koordY()-1);
            
            // Syötetään tämä solmu graafiin, jossa luodaan myös kaari uuden
            // solmun ja edellisen väliin. Pusketaan se myös jonoon.
            gr.insertVertex(edellinen, sl); 
            jono.enqueue(sl);
            
            // System.out.println("Edetty Koilliseen.");
         }
         // Merkataan viimeisenä nykyinen solmu katsotuksi.  
         sl.katsottu(true);
      }
      while(jono.isEmpty() == false);
      
      System.out.println("Leveyshaku valmis.");
   }
   
} 
