package aputietorakenteet;

import graafi.Solmu;
import graafi.Kaari;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: MinHeap.java                                                    #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
#          Muokattu TIETA6-viikkoharjoitusten malliratkaisusta(MinHeap.java)#
#          jonka on tehnyt kurssin henkilökunta: (martti.juhola@tuni.fi)    #
#           + mahdollisesti muut                                            #
# Kuvaus: Taulukkoon perustuva minimikeko-rakenne Kaarien järjestämiseen.   #
# Muuta:  Hyödynnetään minimivirittävän puun muodostamisessa.               #
#############################################################################
*/
public class MinHeap {
   
   // Juuren indeksi.
   private static final int root = 1;
   // Viimeisen kaaren indeksi.
   private int last; 
   // Säiliö kaarille.
   private Kaari[] table;
    
   /*
    * Luodaan uusi keko halutulla koolla.
    */
   public MinHeap (int maxElements) {
      last  = 0;
      table = new Kaari[maxElements+1];
   }

   /*
    * Palauttaa true jos keko on tyhjä.
    */
   public boolean isEmpty () {
      return last == 0;
   }
  
   /*
    * Palauttaa true jos keko on täynnä
    */
   public boolean isFull () {
      return last == table.length-1;
   }

   /*
    * Palauttaa keon elementtien määrän.
    */
   public int elements () {
      return last;
   }

   /*
    * Vaihtaa elementtien (kaarien) paikkaa 
    */
   private void swap (int i, int j) {
      Kaari tmp = table[i];
      table[i] = table[j];
      table[j] = tmp;
   }
 
   /*
    * Palauttaa elementin vanhemman indeksin.
    */
   private static int parent (int i) {
      return i/2;
   }

   /*
    * Palauttaa vasemmanpuoleisen lapsen indeksin.
    */
   private static int leftChild (int i) {
      return i*2;
   }
   
   /*
    * Palauttaa oikeanpuoleisen lapsen indeksin.
    */
   private static int rightChild (int i) {
      return i*2+1;
   }

   /*
    * Poistaa ja palauttaa pienimmän painon sisältävän kaaren.
    */
   public Kaari extractMin (){ 
      
      Kaari result = null;
      
      if (!isEmpty()) {
         result = table[root];
         table[root] = table[last];
         --last;
         percolateDown (root);
	   }
      else {
         System.out.println ("ERROR: extract from empty queue.");
      }
      return result;
   }
    
   /*
    * Siirretään valittua kaarta alaspäin keossa kunnes löydetään isompi paino
    * tai saavutetaan pohja.
    */
   private int percolateDown (int idx) {
      while (((leftChild(idx) <= last) 
            && (table[idx].paino()) > table[leftChild(idx)].paino())
            || ((rightChild(idx) <= last) 
            && (table[idx].paino() > table[rightChild(idx)].paino()))) {
         int swapidx = leftChild(idx);
         if ((rightChild (idx) <= last)
            && (table [leftChild (idx)].paino() > table[rightChild (idx)].paino()))
            swapidx = rightChild (idx);
         swap (idx, swapidx);
         idx = swapidx;
      }
      return idx;
   }

   /*
    * Syötetään uusi kaari kekoon viimeiseksi ja viedään sitä ylöspäin kunnes
    * sille kuuluva paikka löytyy.
    */
   public void insert (Kaari kaari) {
      if (! isFull ()) {
         int i;
         last++;  
         table[last] = kaari;
         percolateUp (last);
	   }
      else
         System.out.println ("ERROR: insertion into a full heap.");
   }

   /*
    * Viedään kaarta ylöspäin keossa kunnes oikea paikka löytyy.
    */
   private int percolateUp (int idx) {
      while ((idx != root) && (table[idx].paino() <= table[parent(idx)].paino())) {
         swap (idx, parent(idx));
         idx = parent(idx);
	   }
      return idx;
   }
}


