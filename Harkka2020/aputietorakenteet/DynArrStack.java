package aputietorakenteet;

import graafi.Solmu;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: DynArrStack.java                                                #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
#          Muokattu TIETA6-viikkoharjoitusten malliratkaisuista, jotka on   #
#          tehnyt kurssin henkilökunta (martti.juhola@tuni.fi) + muut.      #
# Kuvaus: Taulukkoon perustuva Pino-rakenne jonka koko muuttuu dynaamisesti.#
# Muuta:  Hyödynnetään syvyyshaussa (Syvyyshaku.java)                       #
#############################################################################
*/
public class DynArrStack {
   // Taulukon nykyinen koko.
	private int N; 
   // Viite solmu-luokkaan.
   private Solmu solmu;
   // Taulukko johon voidaan sijoittaa solmuja.
	private Solmu[] A;
	// Solmujen nykyinen määrä.
	private int n;

   /*
    * Rakentaja alustaa taulukon koon yhteen, alkioiden määrän nollaan 
    * ja muodostaa uuden taulukon lähtöarvoilla.
    */
	public DynArrStack() {
		N = 1;

		n = 0;
		A = new Solmu[N];
	}

	/*
    * Palauttaa 'true' jos solmujen määrä on nolla.
    */
	public boolean isEmpty() {
		return n == 0;
	}

	/*
    * Palauttaa taulukon nykyisen koon.
    */
	public int arraySize() {
		return N;
	}

   /*
    * Metodi Solmun lisäämiseen pinon päällimmäiseksi.
    * Jos Solmujen määrä saavuttaa taulukon koon, luodaan uusi tuplakokoinen
    * taulukko johon sijoitetaan vanhat solmut. 
    */
	public void push(Solmu x) {

      // Solmujen määrä on saavuttanut taulukon koon, luodaan uusi taulukko.
		if( n == N ) {
      	
         // Alustetaan uusi tuplakokoinen taulukko.
         N = 2*N;
			Solmu[] B = new Solmu[N];
         
         // Kopioidaan olemassaolevat Solmut uuteen taulukkoon.
			for( int i = 0; i < n; i++ ) {
				B[i] = A[i];
				A[i] = null;
			}
         // Korvataan alkuperäinen taulukko uudella.
			A = B;
			B = null;

		}
      // Lisätään uusi Solmu viimeiseksi. 
		A[n++] = x;
	}

   /*
    * Poistaa ja palauttaa pinon päällimmäisen Solmun. Jos Solmujen määrä on
    * poiston jälkeen neljäsosa taulukon koosta, kopioidaan pino uuteen,
    * pienempään taulukkoon. 
    */
   @SuppressWarnings({"unchecked"})
	public Solmu pop () {
      
      // Alustetaan myöhemmin palautettava solmu.
		Solmu result = null;
      
		if(!isEmpty()) {
         // Sijoitetaan viimeinen solmu viitteeseen.
			result = A[--n];
			A[n] = null;

         // Jos taulukon solmujen määrä on liian pieni, muodostetaan uusi 
         // pienempi taulukko.
			if(n == (N/4) && (N >= 2)) {
            
            // Alustetaan uusi taulukko jonka koko on puolet edellisen koosta.
				N = N/2;
				Solmu[] B = new Solmu[N];
            
            // Kopioidaan olemassaolevat Solmut uuteen taulukkoon.   
				for( int i = 0; i < n; i++ ) {

					B[i] = A[i];
					A[i] = null;

				}
            // Korvataan alkuperäinen taulukko uudella.
				A = B;
            B = null;

			}
		}
      // Palauttaa Solmun (tai null-arvon jos solmua ei poistettu). 
		return result;
	}
   /*
    * Palauttaa pinon päällimmäisen alkion poistamatta sitä.
    */
   public Solmu peek() {
    	
      Solmu result = null;
      
      if(!isEmpty()){   
         result = A[--n];
      } 
      return result;
   }
}
