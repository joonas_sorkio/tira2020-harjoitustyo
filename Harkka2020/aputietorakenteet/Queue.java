package aputietorakenteet;

import graafi.Solmu;

/*
#############################################################################
# TIETA6 Tietorakenteet (Syksy 2020)                                        #
# Harjoitustyö                                                              #
# Tiedosto: Queue.java                                                      #
# Oppilas: Joonas Sorkio (joonas.sorkio@tuni.fi).                           #
#          Muokattu TIETA6-viikkoharjoitusten malliratkaisusta (Deque.java) #
#          jonka on tehnyt kurssin henkilökunta: (martti.juhola@tuni.fi)    #
#           + muut                                                          #
# Kuvaus: Taulukkoon perustuva Solmujaa sisältävä jono-rakenne              #
# Muuta:  Hyödynnetään leveyshaussa (Leveyshaku.java)                       #
#############################################################################
*/
public class Queue {
   // Alustetaan jonon kooksi kuvan pikselien maksimimäärä. Vähempikin riittäisi
   // mutta tällä varmistetaan, että kaikki kuvan pikselit(Solmut) voi lisätä
   // tarvittaessa jonoon ja yksittäisiä tapauksia ei käsitellä montaa kertaa 
   // uudelleen, siinä tapauksessa jos jono täyttyisi. 
	public static final int defaultN = 1166400;
   
   // Taulukon koko.
	private int N;
   // Viite ensimmäiseen Solmuun.
	private int first;
   // Viite viimeiseen Solmuun.
	private int rear;
   // Viite Solmujen määrään.
	private int items;
	// Viite Solmuja sisältävään taulukkoon. 
	private Solmu[] Q;
	
	public Queue() {
		this(defaultN);
	}
	/*
    * Jonon rakentaja, alustetaan kaikki viitteet alkuarvoilla ja luodaan uusi 
    * Solmu-taulukko.
    */
	public Queue(int size) {
		first = 0;
		rear = 0;
		items = 0;
		N = size;
		Q = new Solmu[N];
	}
	
	/*
    * Palauttaa 'true' jos solmujen määrä on nolla.
    */
	public boolean isEmpty ()
	{
		return items == 0;
	}

   /*
    * Poistaa ja palauttaa jonon ensimmäisen Solmun tai null-arvon jos
    * jono on tyhjä.
    */
	public Solmu dequeue() {
      
      // Alustetaan myöhemmin palautettava solmu.
		Solmu result = null;
      
      // Poistetaan ensimmäinen alkio ja asetetaan seuraava alkio ensimmäiseksi.
		if (!isEmpty ()) {
			result = Q[first];
			Q[first] = null;
         
         // Käytetään rengastusta tarvittaessa ja vähennetään Solmujen määrää.
			first = (first + 1) % N;
			items--;
		}
      // Palautetaan Solmu. 
		return result;
	}

   /*
    * Lisää uuden Solmun jonon viimeiseksi.
    */
	public void enqueue (Solmu o) {
      
	   // Lisätään uusi Solmu mikäli jono ei ole täynnä.
      if (items < N) {
         Q[rear] = o;
         // Käytetään myös täällä rengastusta.
         rear = (rear + 1) % N;
         items++;
	   }
	}
}
